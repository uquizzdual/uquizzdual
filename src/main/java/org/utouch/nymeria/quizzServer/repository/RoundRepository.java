package org.utouch.nymeria.quizzServer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import org.utouch.nymeria.quizzServer.models.Round;

public interface RoundRepository extends JpaRepository<Round, Long> {
	
	@Modifying
	@Query("update Round r set r.reply_player = ?1 where r.id = ?2")
	@Transactional
	void addPlayerReply(int reply_number, Long roundID);
	
	@Modifying
	@Query("update Round r set r.reply_opponent = ?1 where r.id = ?2")
	@Transactional
	void addOpponentReply(int reply_number, Long roundID);
	
	@Query("SELECT r FROM Round r WHERE r.gameID = ?1")
	List<Round> findByGame(Long gameID);
	
}
