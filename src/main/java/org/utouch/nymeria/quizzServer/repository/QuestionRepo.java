package org.utouch.nymeria.quizzServer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.utouch.nymeria.quizzServer.models.Langue;
import org.utouch.nymeria.quizzServer.models.Question;
import org.utouch.nymeria.quizzServer.models.Theme;

public interface QuestionRepo extends JpaRepository<Question, Long> {
	List<Question> findByQuestionTheme(Theme t);
	@Query("SELECT q FROM Question q WHERE q.questionTheme = ?1 AND q.langue = ?2")
	List<Question> findByQuestionThemeAndByLangue(Theme t, Langue l);
	
	@Query("SELECT q FROM Question q WHERE q.provenance = ?1 AND q.idProvenance = ?2")
	List<Question> findByProvenance(String provenance, String idProvenance);
}
