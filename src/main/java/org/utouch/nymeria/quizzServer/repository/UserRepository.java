package org.utouch.nymeria.quizzServer.repository;

import java.util.Date;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import org.utouch.nymeria.quizzServer.models.User;


@Transactional
public interface UserRepository extends JpaRepository<User, Long> {
	
	User findByLoginStartsWithIgnoreCase(String login);
	User findById(Long id);
	User findByAuthToken(String token);
	
	@Modifying
	@Query("update User u set u.lastActivity = ?1 where u.authToken = ?2")
	void updateLastActivity(Date lastActivity, String authToken);
	
	@Query("SELECT u FROM User u WHERE u.lastActivity >= ?1 AND u.authToken != ?2")
	Set<User> findActiveUsers(Date beginDate, String authToken);
	
	
}
