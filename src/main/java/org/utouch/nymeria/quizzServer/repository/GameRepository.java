package org.utouch.nymeria.quizzServer.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.utouch.nymeria.quizzServer.models.Game;
import org.utouch.nymeria.quizzServer.models.User;

public interface GameRepository extends JpaRepository<Game, Long> {
	List<Game> findByPlayer(User player);
	
	@Query("SELECT g FROM Game g WHERE player.authToken = ?1 OR opponent.authToken = ?1 ORDER BY g.lastActivity DESC ")
	List<Game> findGamesForPlayer(String authToken);
	
	@Query("SELECT g FROM Game g WHERE (player.authToken = ?1 OR opponent.authToken = ?1) AND g.ended = false ORDER BY g.lastActivity DESC ")
	List<Game> findNotEndedGameByPlayer(String authToken);
	
	@Modifying
	@Query("update Game g set g.ended = true, g.lastActivity = ?2 WHERE g.id = ?1")
	@Transactional
	void finishGame(Long gameID, Date date);
	
	@Modifying
	@Query("update Game g set g.lastActivity = ?2 WHERE g.id = ?1")
	@Transactional
	void updateActivity(Long gameID, Date now);
}
