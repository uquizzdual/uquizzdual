package org.utouch.nymeria.quizzServer.controllers;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.utouch.nymeria.quizzServer.models.Game;
import org.utouch.nymeria.quizzServer.models.User;
import org.utouch.nymeria.quizzServer.repository.GameRepository;
import org.utouch.nymeria.quizzServer.repository.UserRepository;

@RestController
@RequestMapping("/user")
public class UserController {
	private static transient final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	@Autowired private UserRepository userRepo;
	@Autowired private GameRepository gameRepo;


	@RequestMapping(method= {RequestMethod.POST})
	@ResponseBody
	public User createUser(@RequestParam(value="login", defaultValue="hello") String login, 
			@RequestParam(value="password", defaultValue="World") String password, 
			@RequestParam(value="email", defaultValue="noemail") String email) {
		User u = new User(login, password, email);
		return userRepo.save(u);
	}

	@RequestMapping(value="logme", method= {RequestMethod.POST})
	@ResponseBody
	public User LoginUser(@RequestParam(value="login", defaultValue="hello") String login, 
			@RequestParam(value="password", defaultValue="World") String password) {
		LOGGER.debug("New authentification request receive");
		User u =  userRepo.findByLoginStartsWithIgnoreCase(login);
		if(u == null) {
			LOGGER.error("Cannot find request user with pseudo = " + login);
			return null;
		}
		if (u.isValidPassword(password)) {
			userRepo.updateLastActivity(new Date(), u.getAuthToken());
			return u;
		} else {
			LOGGER.error("authentification failled : wrong password");
			return null ;
		}
	}


	@RequestMapping(value="get/{login}", method={RequestMethod.GET})
	@ResponseBody
	public User getUserByLogin(@PathVariable String login) {
		User user = userRepo.findByLoginStartsWithIgnoreCase(login);
		if (user != null ) {
			return userRepo.findByLoginStartsWithIgnoreCase(login);
		} else {
			return null;
		}
	}

	@RequestMapping(value="{token}", method={RequestMethod.GET})
	@ResponseBody
	public User getUserByToken(@PathVariable String token) {
		User user = userRepo.findByAuthToken(token);
		if (user != null) {
			return user;
		} else {
			return null;
		}
	}

	@RequestMapping(value="activity/{token}", method={RequestMethod.GET})
	@ResponseBody
	public User updateLastActivity(@PathVariable String token) {

		Date lastActivity = new Date();
		userRepo.updateLastActivity(lastActivity, token);
		User user = userRepo.findByAuthToken(token);

		if (user != null) {
			return user;
		} else {
			return null;
		}
	}

	@RequestMapping(value="findActive/{token}", method={RequestMethod.GET})
	@ResponseBody
	public Set<User> findActiveUsers(@PathVariable String token) {

		// find active users in last 6 minutes
		Date beginDate = new Date(System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(10));
		//Date endDate = new Date();

		userRepo.updateLastActivity(new Date(), token);

		Set<User> actives_users = userRepo.findActiveUsers(beginDate,token);


		// add only the player that are not playing with me
		List<Game> games = gameRepo.findNotEndedGameByPlayer(token);
		Set<User> not_finished_player = new HashSet<User>();
		for (Game g : games) {
			// add the player as my player_not_finished_list
			if(g.getOpponent().getAuthToken().equalsIgnoreCase(token)) {
				not_finished_player.add(g.getPlayer());
			} else {
				not_finished_player.add(g.getOpponent());
			}
		}
		Set<User> not_present = new HashSet<User>(actives_users);
		not_present.removeAll(not_finished_player);
		LOGGER.warn("there is "+ not_present.size()+" opponent found");	
		// in case there is no active user, let chose the last one
		if (not_present.size() == 0) {
			for(User u : not_finished_player) {
				if (u.getAuthToken().equalsIgnoreCase(token) == false) {
					not_present.add(u);
					break;
				}
			}
		}
		LOGGER.warn("there is "+ not_present.size()+" opponent found");
		return not_present;

	}


}
