package org.utouch.nymeria.quizzServer.controllers;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.utouch.nymeria.quizzServer.models.Game;
import org.utouch.nymeria.quizzServer.models.Langue;
import org.utouch.nymeria.quizzServer.models.Question;
import org.utouch.nymeria.quizzServer.models.Round;
import org.utouch.nymeria.quizzServer.models.Theme;
import org.utouch.nymeria.quizzServer.repository.GameRepository;
import org.utouch.nymeria.quizzServer.repository.QuestionRepo;
import org.utouch.nymeria.quizzServer.repository.RoundRepository;
import org.utouch.nymeria.quizzServer.repository.UserRepository;

@RestController
@RequestMapping("/round")
public class RoundController {
	private static transient final Logger LOGGER = LoggerFactory.getLogger(RoundController.class);

	private static final Random RANDOM = new Random();    
	/**
	 * Pick n numbers between 0 (inclusive) and k (inclusive)
	 * While there are very deterministic ways to do this,
	 * for large k and small n, this could be easier than creating
	 * an large array and sorting, i.e. k = 10,000
	 */
	public static Set<Integer> pickRandom(int n, int k) {
		final Set<Integer> picked = new HashSet<>();
		while (picked.size() < n) {
			picked.add(RANDOM.nextInt(k + 1));
		}
		return picked;
	}
	@Autowired private GameRepository gameRepo;
	@Autowired private RoundRepository roundRepo;
	@Autowired private QuestionRepo questionRepo;
	@Autowired private UserRepository userRepo;

	@RequestMapping(value="/", method={RequestMethod.GET})
	@ResponseBody
	public  List<Round>  getRounds() {
		return roundRepo.findAll();
	}

	/**
	 * This method is used to create a new round between two player
	 * @param gameID
	 * @param theme
	 * @param langue
	 * @return
	 */
	@RequestMapping(method={RequestMethod.POST})
	@ResponseBody
	public  Round  startNewRound(
			@RequestParam(value="gameID", defaultValue="noGameID") String gameIDString,
			@RequestParam(value="theme", defaultValue="noTheme") String theme,
			@RequestParam(value="langue", defaultValue="noLangue") String langue
			) {
		// specify the question THEME
		if (theme.equalsIgnoreCase("noTheme") == false) {
			Theme t = Theme.valueOf(theme);

			// get question langue
			Langue l;
			if (langue.equalsIgnoreCase("noLangue") == true) {
				l = Langue.ENG;
			} else {
				l = Langue.valueOf(langue);
			}
			
			// update game activity
			Long gameID = Long.parseLong(gameIDString);
			gameRepo.updateActivity(gameID, new Date());
			

			// find a question randoomly
			List<Question> questions = questionRepo.findByQuestionThemeAndByLangue(t, l);
			Round r = null;

			// special case there is less than 2 question in database
			if (questions.size() < 3) {
				roundRepo.save(new Round(gameID, questions.get(0)));
				roundRepo.save(new Round(gameID, questions.get(1)));
				r = roundRepo.save(new Round(gameID, questions.get(0)));

				// special case there is only 3 question in database
			} else if (questions.size() < 4) {
				for (int i=0; i < 3; i++ ) {
					if (i < 2) {
						roundRepo.save(new Round(gameID, questions.get(i)));
					} else  {
						r = roundRepo.save(new Round(gameID, questions.get(i)));

					}
				}
				
			} else {
				Set<Integer> ids = pickRandom(3, questions.size()-1); 
				for (Integer i : ids) {
					// create new round
					r = roundRepo.save(new Round(gameID, questions.get(i)));
				}
			}

			return r;
		} else {
			LOGGER.debug("No theme given for the new round");
			return null;
		}
	}

	@RequestMapping(value="/{roundID}", method={RequestMethod.GET})
	@ResponseBody
	public  Round getRoundByID(@PathVariable String roundID) {
		return roundRepo.findOne(Long.parseLong(roundID));

	}

	@RequestMapping(value="/byGame/{gameID}", method={RequestMethod.GET})
	@ResponseBody
	public  List<Round> getRoundByGame(@PathVariable String gameID) {
		return roundRepo.findByGame(Long.parseLong(gameID));

	}


	/** This method is used for set a player's response to a specific question
	 * 
	 * @param gameID the ID of the game
	 * @param userToken the token that identify the player that add the reply
	 * @param roundID the round number
	 * @param replyNumber the reply of the user (should be betewen 1 and 4)
	 * @return the state of current party
	 */
	@RequestMapping(value="reply/{gameID}/{roundID}/{userToken}/{replyNumber}", method={RequestMethod.GET})
	@ResponseBody
	public  Round addReply(
			@PathVariable String gameID,
			@PathVariable String userToken,
			@PathVariable String roundID, 
			@PathVariable String replyNumber) {

		Game g = gameRepo.findOne(Long.parseLong(gameID));
		Round r = roundRepo.findOne(Long.parseLong(roundID));

		if(g== null || r == null) {
			return null;
		}
		
		g.setLastActivity(new Date());
		
		userRepo.updateLastActivity(new Date(), userToken);
		int reply_number = Integer.parseInt(replyNumber);
		// if it's an opponent reply
		if (g.getOpponent().getAuthToken().equalsIgnoreCase(userToken)) {
			r.setReply_opponent(reply_number);
			// if it's the good answer
			if (reply_number == 1) {
				g.setOpponent_score(g.getOpponent_score()+1);
				gameRepo.save(g);
			}
		} else if(g.getPlayer().getAuthToken().equalsIgnoreCase(userToken)){
			r.setReply_player(reply_number);

			// if it's the good answer
			if(reply_number == 1) {
				g.setPlayer_score(g.getPlayer_score()+1);
				gameRepo.save(g);
			}
		} else {
			LOGGER.error("User with token = "+userToken +" try to add a response, but he's not a member of this game");
			return null;
		}
		roundRepo.save(r);
		userRepo.updateLastActivity(new Date(), userToken);

		return r;
	}

}

