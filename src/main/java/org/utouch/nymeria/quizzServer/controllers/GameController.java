package org.utouch.nymeria.quizzServer.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.utouch.nymeria.quizzServer.models.Game;
import org.utouch.nymeria.quizzServer.models.User;
import org.utouch.nymeria.quizzServer.repository.GameRepository;
import org.utouch.nymeria.quizzServer.repository.UserRepository;

@RestController
@RequestMapping("/game")
public class GameController {
	private static transient final Logger LOGGER = LoggerFactory.getLogger(GameController.class);

	@Autowired private  GameRepository gameRepo;
	@Autowired private  UserRepository userRepo;


	@RequestMapping(value="/", method={RequestMethod.GET})
	@ResponseBody
	public  List<Game>  getGames() {
		return gameRepo.findAll();
	}

	@RequestMapping(value="/byPlayer/{userToken}", method={RequestMethod.GET})
	@ResponseBody
	public  List<Game>  findGameByUser(@PathVariable String userToken) {
		userRepo.updateLastActivity(new Date(), userToken);
		return gameRepo.findGamesForPlayer(userToken);
	}

	@RequestMapping(value="/finish", method= {RequestMethod.POST})
	@ResponseBody
	public Game finishGame(@RequestParam(value="gameID", defaultValue="noGameID") String gameID,
			@RequestParam(value="userToken", defaultValue="noToken") String userToken) {
		Game g = gameRepo.findOne(Long.parseLong(gameID));
		if (g.getOpponent().getAuthToken().equalsIgnoreCase(userToken) == true) {
			LOGGER.debug("Opponent ("+g.getOpponent().getLogin()+") send finis request for the gameID ="+ gameID);
			gameRepo.finishGame(Long.parseLong(gameID), new Date());

		} else if (g.getPlayer().getAuthToken().equalsIgnoreCase(userToken) == true) {
			LOGGER.debug("Player ("+g.getPlayer().getLogin()+") send finis request for the gameID ="+ gameID);
			gameRepo.finishGame(Long.parseLong(gameID), new Date());

		} else {
			LOGGER.error("Somone try to finish a game, but is not a player, authToken = "+userToken);
			return null;
		}
		return g;		
	}


	@RequestMapping(method= {RequestMethod.POST})
	@ResponseBody
	public Game newGame(@RequestParam(value="ownerToken", defaultValue="noOwnerToken") String ownerToken, 
			@RequestParam(value="opponentPseudo", defaultValue="noOpponentPseudo") String opponentPseudo) {
		List<User> tuple = getPlayerTuple(ownerToken, opponentPseudo);

		if (tuple.size() == 2) {
			//System.out.println("new party started");
			Game g = new Game(tuple.get(0), tuple.get(1));
			gameRepo.save(g);
			return g;
		}
		//System.out.println("Cannot start new party because can't find player requested");
		return null;
	}

	@RequestMapping(value="/{gameID}", method={RequestMethod.GET})
	@ResponseBody
	public  Game getGameByID(@PathVariable String gameID) {
		return gameRepo.findOne(Long.parseLong(gameID));
	}


	private List<User> getPlayerTuple(String ownerToken, String opponentPseudo) {
		List<User> tuple = new ArrayList<User>();

		User owner = userRepo.findByAuthToken(ownerToken);
		if (owner != null) {
			tuple.add(owner);

			if (opponentPseudo.equalsIgnoreCase("noOpponentPseudo") == false) {
				User opponent = userRepo.findByLoginStartsWithIgnoreCase(opponentPseudo);
				if (opponent != null) {
					tuple.add(opponent);
				} else {
					LOGGER.debug("cannot find opponent by pseudo");
				}
			} else {
				LOGGER.debug("don't have the opponent pseudo (default value)");
			}
		} else {
			LOGGER.debug("cannot find owner by token");
		}
		return tuple;
	}
}
