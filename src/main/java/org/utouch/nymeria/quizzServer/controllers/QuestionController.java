package org.utouch.nymeria.quizzServer.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.utouch.nymeria.quizzServer.models.Langue;
import org.utouch.nymeria.quizzServer.models.Question;
import org.utouch.nymeria.quizzServer.models.Theme;
import org.utouch.nymeria.quizzServer.repository.QuestionRepo;
import org.utouch.nymeria.quizzServer.repository.UserRepository;

@RestController
@RequestMapping("/question")
public class QuestionController {
	private static transient final Logger LOGGER = LoggerFactory.getLogger(QuestionController.class);

	@Autowired QuestionRepo questionRepo;
	@Autowired UserRepository userRepo;

	@RequestMapping(value="/", method={RequestMethod.GET})
	@ResponseBody
	public  List<Question>  getQuestions() {
		return questionRepo.findAll();
	}

	@RequestMapping(value="/", method={RequestMethod.POST})
	@ResponseBody
	public Question  createQuestion(
			@RequestParam(value="ownerToken", defaultValue="noOwnerToken") String ownerToken,
			@RequestParam(value="theme", defaultValue="noTheme") String theme,
			@RequestParam(value="langue", defaultValue="noLangue") String langue,
			@RequestParam(value="question", defaultValue="") String question,
			@RequestParam(value="r1", defaultValue="") String r1,
			@RequestParam(value="r2", defaultValue="") String r2,
			@RequestParam(value="r3", defaultValue="") String r3,
			@RequestParam(value="r4", defaultValue="") String r4,
			@RequestParam(value="provenance", defaultValue="") String provenance,
			@RequestParam(value="idProvenance", defaultValue="") String idProvenance
			) {
		Long userID = null;
		if (ownerToken.equalsIgnoreCase("noOwnerToken")) {
			LOGGER.warn("no user specified");
			//userID = (long) 1;
		} else {
			userID = userRepo.findByAuthToken(ownerToken).getId();
		}

		if (userID != null && theme.equalsIgnoreCase("noTheme") == false) {
			Theme t = Theme.valueOf(theme);

			// get question langue
			Langue l;
			if (langue.equalsIgnoreCase("noLangue") == true) {
				l = Langue.ENG;
			} else {
				l = Langue.valueOf(langue);
			}
			if(question.length() > 0 && r1.length() > 0 && r2.length() > 0 && r3.length() > 0 && r4.length() > 0) {
				if (provenance.length() > 0 && idProvenance.length() > 0) {
					List<Question> questions = questionRepo.findByProvenance(provenance, idProvenance);
					if (questions != null && questions.size() > 0) {
						LOGGER.warn("question already exist");
						return null;
					}
				}
				Question q = new Question(userID, t, question, r1,r2,r3,r4, l);
				q.setProvenance(provenance);
				q.setIdProvenance(idProvenance);
				questionRepo.save(q);
				return q;
			} else {
				LOGGER.warn("Try to add question, data are not setted");
				return null;
			}
		} else {
			LOGGER.warn("no theme specified");
		}


		return null;
	}
}
