package org.utouch.nymeria.quizzServer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.utouch.nymeria.quizzServer.models.Game;
import org.utouch.nymeria.quizzServer.models.Langue;
import org.utouch.nymeria.quizzServer.models.Question;
import org.utouch.nymeria.quizzServer.models.Theme;
import org.utouch.nymeria.quizzServer.models.User;
import org.utouch.nymeria.quizzServer.repository.GameRepository;
import org.utouch.nymeria.quizzServer.repository.QuestionRepo;
import org.utouch.nymeria.quizzServer.repository.RoundRepository;
import org.utouch.nymeria.quizzServer.repository.UserRepository;

@SpringBootApplication
public class Application extends SpringBootServletInitializer {
 
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

	private static final Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public CommandLineRunner loadData(UserRepository userRepo, GameRepository gameRepo, QuestionRepo questionRepo, RoundRepository roundRepo) {
		return (args) -> {

			/*//save couple of users
			userRepo.save(new User("morgan","motdepasse", "morgan-durand@laposte.net"));
			User jesaispas = userRepo.save(new User("jesaispas","motdepasse"));
			//userRepo.save(new User("michael","koza"));


			User morgan = userRepo.findByLoginStartsWithIgnoreCase("morgan");


			Question q1 = new Question(morgan.getId(), Theme.CINEMA, 
					"Which of these artists is/are portrayed in this music biopic? \n La Bamba (Lou Diamond Phillips 1987)",
					"Ritchie Valens", 
					"Buddy Holly", 
					"Eddie Cochran", 
					"Ricky Nelson",
					Langue.ENG);


			Question q2 = new Question(morgan.getId(), Theme.CINEMA, 
					"Which character from a Disney film is described here?\n The common name of Arthur, the 12 year old orphan and protagonist (The Sword in the Stone)",
					"Wart", 
					"Taran", 
					"Abu", 
					"Scar",
					Langue.ENG);


			Question q3 = new Question(morgan.getId(), Theme.CINEMA, 
					"Which of these is a fictional city or town in this movie? \n The Simpsons Movie",
					"Wart", 
					"Taran", 
					"Abu", 
					"Scar",
					Langue.ENG);


			Question q4 = new Question(morgan.getId(), Theme.CINEMA, 
					"These movie stars were all born in which year? \n Minnie Driver, Tina Fey and Joseph Fiennes",
					"1970",
					"1975",
					"1965",
					"1960",
					Langue.ENG);

			Question q5 = new Question(morgan.getId(), Theme.CINEMA, 
					"What type of hat did Indiana Jones usually wear?",
					"Fedora",
					"Breton",
					"Homburg",
					"Panama",
					Langue.ENG);



			Question q6 = new Question(morgan.getId(), Theme.GEOGRAPHIE, 
					"This is the capital of which country?",
					"Qatar",
					"Togo",
					"Tuvalu",
					"Comoros",
					Langue.ENG);

			Question q7 = new Question(morgan.getId(), Theme.GEOGRAPHIE, 
					"This landmark can be found in or near which of these locations? : The Prado",
					"Madrid",
					"Montevideo",
					"Manila",
					"Santiago",

					Langue.ENG);

			Question q8 = new Question(morgan.getId(), Theme.GEOGRAPHIE, 
					"What is the easternmost national capital city of mainland (excluding islands) Asia?",
					"Seoul",
					"Beijing",
					"Hanoi",
					"Pyongyang",
					Langue.ENG);

			Question q9 = new Question(morgan.getId(), Theme.GEOGRAPHIE, 
					"Where can we find this old region nowadays? : Asia Minor",
					"Turkey",
					"Japan",
					"Iran",
					"India",
					Langue.ENG);

			Question q10 = new Question(morgan.getId(), Theme.GEOGRAPHIE, 
					"This was a former capital of which country (or territory) between these years? \n Salalah (1932-1970)",
					"Oman",
					"Kuwait",
					"South Yemen",
					"North Yemen",
					Langue.ENG);
			
			Question q11 = new Question(morgan.getId(), Theme.SCIENCE, 
					"This scientist or inventor lived between which of these years? \n Nikola Tesla",
					"1856-1943",
					"1556-1643",
					"1656-1743",
					"1756-1843",
					Langue.ENG);
			
			Question q12 = new Question(morgan.getId(), Theme.SCIENCE, 
					"This is a scientific name for a collector of which objects? Bibliophilist",
					"Books",
					"Spoons",
					"Banknotes and cheques",
					"Postcards",

					Langue.ENG);
			Question q13 = new Question(morgan.getId(), Theme.SCIENCE, 
					"This is the study or science of what? \n geology ",
					"study of earth's crust",
					"study of earthquakes",
					"study of human material remains",
					"science of knowledge or wisdom",
					Langue.ENG);
			
			Question q14 = new Question(morgan.getId(), Theme.SCIENCE, 
					"These were the living years of which scientist or inventor? 1733-1804",
					"Joseph Priestly",
					"Paracelsus",
					"Louis Daguerre",
					"Charles Babage",
					Langue.ENG);
			
			Question q15 = new Question(morgan.getId(), Theme.SCIENCE, 
					"This scientist or inventor lived between which of these years? \n Antony van Leeuwenhoek",
					"1632-1723",
					"1532-1623",
					"1732-1823",
					"1832-1923",
					Langue.ENG);
			
			Question q16 = new Question(morgan.getId(), Theme.MUSIQUE, 
					"Which artist or group can also be heard on this duet? \n <b> Move Ya Body - Nina Sky and ...? </b>",
					"Jabba",
					"Rakim",
					"Twista",
					"Syleena Johnson",
					Langue.ENG);
			
			Question q17 = new Question(morgan.getId(), Theme.MUSIQUE, 
					"What was the first US or UK Top 40 <b>Nelly's</b> hit",
					"Country Grammar",
					"Hot In Herre",
					"Dilemma",
					"Ride Wit Me",
					Langue.ENG);
			
			Question q18 = new Question(morgan.getId(), Theme.MUSIQUE, 
					"Who had a hit with the song : \"Call Me When You're Sober\"?",
					"Evanescence",
					"Pink",
					"Razorlight",
					"Keane",
					Langue.ENG);
			
			Question q19 = new Question(morgan.getId(), Theme.MUSIQUE, 
					"Who composed <b>\"Rumanian Dances From Hungary\"</b> piece of classical music?",
					"Bela Bartok",
					"Sergey Rachmaninoff",
					"Alexander Borodin",
					"Dmitry Shostakovich",
					Langue.ENG);
		
			

					
			questionRepo.save(q1);
			questionRepo.save(q2);
			questionRepo.save(q3);
			questionRepo.save(q4);
			questionRepo.save(q5);
			questionRepo.save(q6);
			questionRepo.save(q7);
			questionRepo.save(q8);
			questionRepo.save(q9);
			questionRepo.save(q10);
			questionRepo.save(q11);
			questionRepo.save(q12);
			questionRepo.save(q13);
			questionRepo.save(q14);
			questionRepo.save(q15);
			questionRepo.save(q16);
			questionRepo.save(q17);
			questionRepo.save(q18);
			questionRepo.save(q19);
			// create first game
			Game game1 = new Game(morgan,jesaispas);
			gameRepo.save(game1);
			*/

			/*// add first round
			Round r1 = new Round(game1.getId(),q1);
			roundRepo.save(r1);
			// create second round
			Round r2 = new Round(game1.getId(), q2);
			roundRepo.save(r2);
			// create second round
			Round r3 = new Round(game1.getId(), q3);
			roundRepo.save(r3);


			//game1.getRounds().add(r1);
			//gameRepo.save(game1);

			roundRepo.addPlayerReply(3, r1.getId());
			roundRepo.addOpponentReply(1, r1.getId());




			//game1.getRounds().add(r2);
			gameRepo.save(game1);

			roundRepo.addPlayerReply(4, r2.getId());*/


		};
	}

}