package org.utouch.nymeria.quizzServer.models;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.keygen.KeyGenerators;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class User implements Serializable {

	private static final long serialVersionUID = 1L;
	private static transient final Logger log = LoggerFactory.getLogger(User.class);


	@Id
	@GeneratedValue
	private Long id;
	
	private String login;
	
    @JsonIgnore
	private String password;
	
	private String email;
    
	@JsonIgnore
	private String salt = KeyGenerators.string().generateKey();
	
    @Temporal(TemporalType.TIMESTAMP)
	private Date lastActivity;
    
    @Temporal(TemporalType.TIMESTAMP)
	private Date registrationDate = new Date();
	
	@Column(unique=true, length = 128) 
	private String authToken;

	public User() {
		super();
	}

	public User(Long id, String login, String password) {
		super();
		this.id = id;
		this.login = login;
		this.password = generatePassword(password);
		generateToken();
	}

	public User(String login, String password) {
		this.login = login;
		this.password = generatePassword(password);
		generateToken();
	}
	public User(String login, String password, String email) {
		this.login = login;
		this.password = generatePassword(password);
		this.email = email;
		generateToken();
	}
	
	public Boolean isValidPassword(String pass) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12);
		return encoder.matches(pass, password);
		
	}
	
	private void generateToken() {
		StringBuffer alea = new StringBuffer();
		alea.append(UUID.randomUUID().toString());
		alea.append(salt);
		
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			
			md.update(alea.toString().getBytes());
	        byte byteData[] = md.digest();
	 
	        //convert the byte to hex format method 1
	        StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < byteData.length; i++) {
	         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
	        }
	        this.authToken = sb.toString();
	    
		} catch (NoSuchAlgorithmException e) {
			log.debug("an error hapen when try to generate user token", e);
		}
	}
	
	private String generatePassword(String str) {
		
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12);
		
		return encoder.encode(str);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authTooken) {
		this.authToken = authTooken;
	}
	
	public Date getLastActivity() {
		return lastActivity;
	}

	public void setLastActivity(Date lastActivity) {
		this.lastActivity = lastActivity;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	@Override
	public String toString() {
		return String.format("Username[id=%d, login='%s', password='%s']", id,
				login, password);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authToken == null) ? 0 : authToken.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lastActivity == null) ? 0 : lastActivity.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((registrationDate == null) ? 0 : registrationDate.hashCode());
		result = prime * result + ((salt == null) ? 0 : salt.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (authToken == null) {
			if (other.authToken != null)
				return false;
		} else if (!authToken.equals(other.authToken))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastActivity == null) {
			if (other.lastActivity != null)
				return false;
		} else if (!lastActivity.equals(other.lastActivity))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (registrationDate == null) {
			if (other.registrationDate != null)
				return false;
		} else if (!registrationDate.equals(other.registrationDate))
			return false;
		if (salt == null) {
			if (other.salt != null)
				return false;
		} else if (!salt.equals(other.salt))
			return false;
		return true;
	}



}
