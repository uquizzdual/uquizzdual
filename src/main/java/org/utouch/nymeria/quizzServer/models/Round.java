package org.utouch.nymeria.quizzServer.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Round implements Serializable {

	private static final long serialVersionUID = 2L;

	@Id
	@GeneratedValue
	private Long id;

	private int reply_player;
	private int reply_opponent;

	
	private Long gameID;


	@OneToOne(targetEntity=Question.class)
	private Question question;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate = new Date();


	public Round(Long gameID, Question question) {
		super();
		this.question = question;
		this.gameID = gameID;
	}

	public Round(Long gameID, int reply_player, int reply_opponent, Question question) {
		super();
		this.reply_player = reply_player;
		this.reply_opponent = reply_opponent;
		this.question = question;
		this.gameID = gameID;
	}

	public Round() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getReply_player() {
		return reply_player;
	}

	public void setReply_player(int reply_player) {
		this.reply_player = reply_player;
	}

	public int getReply_opponent() {
		return reply_opponent;
	}

	public void setReply_opponent(int reply_opponent) {
		this.reply_opponent = reply_opponent;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Long getGameID() {
		return gameID;
	}

	public void setGameID(Long gameID) {
		this.gameID = gameID;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	
}
