package org.utouch.nymeria.quizzServer.models;

public enum Theme {
	CINEMA,
	ARTS,
	CITATIONS,
	DIVERTISSEMENTS,
	ECONOMIE,
	GEOGRAPHIE,
	MUSIQUE,
	NATURE,
	RELIGION,
	SCIENCE,
	SPORT,
	HISTOIRE;
	
}
