package org.utouch.nymeria.quizzServer.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Question implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long idQuestion;
	
	@Enumerated(EnumType.STRING)
	private Theme questionTheme;
	
	private Long ownerID;
	
	@Enumerated(EnumType.STRING)	
	private Langue langue;
	
	private String questionTexte;
	private String reply_1;
	private String reply_2;
	private String reply_3;
	private String reply_4;
	
	private int reported=0;
	
	private String provenance;
	private String idProvenance;

	
	public Question(Long ownerID, Theme questionTheme, String questionTexte, String reply_1, String reply_2,
			String reply_3, String reply_4) {
		super();
		this.ownerID = ownerID;
		this.questionTheme = questionTheme;
		this.questionTexte = questionTexte;
		this.reply_1 = reply_1;
		this.reply_2 = reply_2;
		this.reply_3 = reply_3;
		this.reply_4 = reply_4;
	}
	public Question(Long ownerID, Theme questionTheme, String questionTexte, String reply_1, String reply_2,
			String reply_3, String reply_4, Langue langue) {
		super();
		this.ownerID = ownerID;
		this.questionTheme = questionTheme;
		this.questionTexte = questionTexte;
		this.reply_1 = reply_1;
		this.reply_2 = reply_2;
		this.reply_3 = reply_3;
		this.reply_4 = reply_4;
		this.langue = langue;
	}

	public Question() {
		super();
	}

	public Long getIdQuestion() {
		return idQuestion;
	}

	public void setIdQuestion(Long idQuestion) {
		this.idQuestion = idQuestion;
	}

	public String getQuestionTexte() {
		return questionTexte;
	}

	public void setQuestionTexte(String questionTexte) {
		this.questionTexte = questionTexte;
	}

	public String getReply_1() {
		return reply_1;
	}

	public void setReply_1(String reply_1) {
		this.reply_1 = reply_1;
	}

	public String getReply_2() {
		return reply_2;
	}

	public void setReply_2(String reply_2) {
		this.reply_2 = reply_2;
	}

	public String getReply_3() {
		return reply_3;
	}

	public void setReply_3(String reply_3) {
		this.reply_3 = reply_3;
	}

	public String getReply_4() {
		return reply_4;
	}

	public void setReply_4(String reply_4) {
		this.reply_4 = reply_4;
	}

	public Theme getQuestionTheme() {
		return questionTheme;
	}

	public void setQuestionTheme(Theme questionTheme) {
		this.questionTheme = questionTheme;
	}

	public Long getOwnerID() {
		return ownerID;
	}

	public void setOwnerID(Long ownerID) {
		this.ownerID = ownerID;
	}
	public Langue getLangue() {
		return langue;
	}
	public void setLangue(Langue langue) {
		this.langue = langue;
	}
	
	public int getReported() {
		return reported;
	}
	public void setReported(int reported) {
		this.reported = reported;
	}
	
	public String getProvenance() {
		return provenance;
	}
	public void setProvenance(String provenance) {
		this.provenance = provenance;
	}
	public String getIdProvenance() {
		return idProvenance;
	}
	public void setIdProvenance(String idProvenance) {
		this.idProvenance = idProvenance;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idProvenance == null) ? 0 : idProvenance.hashCode());
		result = prime * result
				+ ((idQuestion == null) ? 0 : idQuestion.hashCode());
		result = prime * result + ((langue == null) ? 0 : langue.hashCode());
		result = prime * result + ((ownerID == null) ? 0 : ownerID.hashCode());
		result = prime * result
				+ ((provenance == null) ? 0 : provenance.hashCode());
		result = prime * result
				+ ((questionTexte == null) ? 0 : questionTexte.hashCode());
		result = prime * result
				+ ((questionTheme == null) ? 0 : questionTheme.hashCode());
		result = prime * result + ((reply_1 == null) ? 0 : reply_1.hashCode());
		result = prime * result + ((reply_2 == null) ? 0 : reply_2.hashCode());
		result = prime * result + ((reply_3 == null) ? 0 : reply_3.hashCode());
		result = prime * result + ((reply_4 == null) ? 0 : reply_4.hashCode());
		result = prime * result + reported;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Question other = (Question) obj;
		if (idProvenance == null) {
			if (other.idProvenance != null)
				return false;
		} else if (!idProvenance.equals(other.idProvenance))
			return false;
		if (idQuestion == null) {
			if (other.idQuestion != null)
				return false;
		} else if (!idQuestion.equals(other.idQuestion))
			return false;
		if (langue != other.langue)
			return false;
		if (ownerID == null) {
			if (other.ownerID != null)
				return false;
		} else if (!ownerID.equals(other.ownerID))
			return false;
		if (provenance == null) {
			if (other.provenance != null)
				return false;
		} else if (!provenance.equals(other.provenance))
			return false;
		if (questionTexte == null) {
			if (other.questionTexte != null)
				return false;
		} else if (!questionTexte.equals(other.questionTexte))
			return false;
		if (questionTheme != other.questionTheme)
			return false;
		if (reply_1 == null) {
			if (other.reply_1 != null)
				return false;
		} else if (!reply_1.equals(other.reply_1))
			return false;
		if (reply_2 == null) {
			if (other.reply_2 != null)
				return false;
		} else if (!reply_2.equals(other.reply_2))
			return false;
		if (reply_3 == null) {
			if (other.reply_3 != null)
				return false;
		} else if (!reply_3.equals(other.reply_3))
			return false;
		if (reply_4 == null) {
			if (other.reply_4 != null)
				return false;
		} else if (!reply_4.equals(other.reply_4))
			return false;
		if (reported != other.reported)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Question [idQuestion=" + idQuestion + ", questionTheme="
				+ questionTheme + ", ownerID=" + ownerID + ", langue=" + langue
				+ ", questionTexte=" + questionTexte + ", reply_1=" + reply_1
				+ ", reply_2=" + reply_2 + ", reply_3=" + reply_3
				+ ", reply_4=" + reply_4 + "]";
	}

	
	

}
