package org.utouch.nymeria.quizzServer.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Game implements Serializable {
	
	private static final long serialVersionUID = 2L;

	@Id
	@GeneratedValue
	@Column(name="GAME_ID")
	private Long id;
	
	@ManyToOne
	private User player;
	
	@ManyToOne
	private User opponent;
	
	private int player_score=0;
	private int opponent_score=0;
	
	private Boolean ended;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastActivity;
	
	/*@OneToMany(targetEntity=Round.class,
		       fetch=FetchType.LAZY)
	@JoinColumn(name="ROUND_ID", referencedColumnName="GAME_ID")
	//@JoinColumn(name="ROUND_ID")
	private List<Round> rounds;*/
	

	public Game(Long id, User player, User opponent) {
		super();
		this.id = id;
		this.player = player;
		this.opponent = opponent;
		this.ended = false;
		this.startDate = new Date();
		this.lastActivity = new Date();
	}

	public Game(User player, User opponent) {
		super();
		this.player = player;
		this.opponent = opponent;
		this.ended = false;
		this.startDate = new Date();
		this.lastActivity = new Date();
	}

	public Game() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getPlayer() {
		return player;
	}

	public void setPlayer(User player) {
		this.player = player;
	}

	public User getOpponent() {
		return opponent;
	}

	public void setOpponent(User opponent) {
		this.opponent = opponent;
	}

	public Boolean getEnded() {
		return ended;
	}

	public void setEnded(Boolean ended) {
		this.ended = ended;
	}
	
	
	public int getPlayer_score() {
		return player_score;
	}

	public void setPlayer_score(int player_score) {
		this.player_score = player_score;
	}

	public int getOpponent_score() {
		return opponent_score;
	}

	public void setOpponent_score(int opponent_score) {
		this.opponent_score = opponent_score;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public Date getLastActivity() {
		return lastActivity;
	}

	public void setLastActivity(Date lastActivity) {
		this.lastActivity = lastActivity;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ended == null) ? 0 : ended.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lastActivity == null) ? 0 : lastActivity.hashCode());
		result = prime * result + ((opponent == null) ? 0 : opponent.hashCode());
		result = prime * result + opponent_score;
		result = prime * result + ((player == null) ? 0 : player.hashCode());
		result = prime * result + player_score;
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Game other = (Game) obj;
		if (ended == null) {
			if (other.ended != null)
				return false;
		} else if (!ended.equals(other.ended))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastActivity == null) {
			if (other.lastActivity != null)
				return false;
		} else if (!lastActivity.equals(other.lastActivity))
			return false;
		if (opponent == null) {
			if (other.opponent != null)
				return false;
		} else if (!opponent.equals(other.opponent))
			return false;
		if (opponent_score != other.opponent_score)
			return false;
		if (player == null) {
			if (other.player != null)
				return false;
		} else if (!player.equals(other.player))
			return false;
		if (player_score != other.player_score)
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return new String("game "+id+" ; "+ player.getLogin()+" vs " + opponent.getLogin() +" ; is ended = "+ended );
	}

}
